package edu.ucsd.cse110.library;

public class Library implements ILibrary{
	LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
	LocalDate checkinDate = LocalDate.of(20014, 12, 4);
	
	@Override
	public void checkoutPublication(Member mem, Publication pub){
		//checkoutDate = LocalDate.now(PST);
		pub.checkout(mem,checkoutDate);
	}
	
	@Override
	public void returnPublication(Publication pub){
		//checkinDate = LocalDate.now(PST);
		pub.pubReturn(checkinDate);
	}
	
	@Override
	public double getFee(Member mem){
		return mem.getDueFees()
	}
	
	@Override
	public boolean hasFee(Member){
		return (mem.getDueFees() > 0);
	}
	
	@Override
	public boolean checkoutStatus(Publication pub){
		return pub.isCheckout();
	}
	
	@Override
	public Member pubRenter(Publication pub){
		return pub.getMember();
	}
	
	@Override
	public LocalDate pubCheckoutDate(Publication pub){
		return pub.getCheckoutDate();
	}
}
