package edu.ucsd.cse110.library;

public interface ILibrary {
	public void checkoutPublication(Member mem, Publication pub);
	public void returnPublication(Publication pub);
	public double getFee(Member mem);
	public boolean hasFee(Member mem);
	public boolean checkoutStatus(Publication pub);
	public Member pubRenter(Publication pub);
	public LocalDate pubCheckoutDate(Publication pub);
}
